<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
            'HIVE.HiveCptCntAnnoyingPopup',
            'Hivecptcntannoyingpopuprender',
            'hive_cpt_cnt_annoying_popup :: Render'
        );

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('hive_cpt_cnt_annoying_popup', 'Configuration/TypoScript', 'hive_cpt_cnt_annoying_popup');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_hivecptcntannoyingpopup_domain_model_annoyingpopup', 'EXT:hive_cpt_cnt_annoying_popup/Resources/Private/Language/locallang_csh_tx_hivecptcntannoyingpopup_domain_model_annoyingpopup.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_hivecptcntannoyingpopup_domain_model_annoyingpopup');

    }
);
## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder