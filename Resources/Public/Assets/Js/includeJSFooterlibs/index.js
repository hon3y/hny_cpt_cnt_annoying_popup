/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Andreas Hafner, Dominik Hilser, Georg Kathan, Hendrik Kr�ger, Timo Bittner :: teufels GmbH
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 **/

var hive_cpt_cnt_annoying_popup__interval = setInterval(function () {
    if (typeof jQuery == 'undefined') {
    } else {
      if (typeof jQuery == 'function') {
        if (typeof hive_thm_bs__loaded == 'undefined' && typeof teufels_thm_bs__loaded == 'undefined') {
        } else {
          /**
           * Fallback for CORE
           * Not using hive in TYPO3 <=8.6.0
           *
           * There is no hive_thm_bs__loaded variable available ever!
           */
          if (typeof hive_thm_bs__loaded == 'undefined' && typeof teufels_thm_bs__loaded != 'undefined') {
            // we are using core
            hive_thm_bs__loaded = true;
          }

          if (typeof hive_thm_bs__loaded == "boolean" && hive_thm_bs__loaded) {

            if (typeof hive_cfg_typoscript__windowLoad == 'undefined' && typeof teufels_cfg_typoscript__windowLoad == 'undefined') {
            } else {
              /**
               * Fallback for CORE
               * Not using hive in TYPO3 <=8.6.0
               *
               * There is no hive_thm_bs__loaded variable available ever!
               */
              if (typeof hive_cfg_typoscript__windowLoad == 'undefined' && typeof teufels_cfg_typoscript__windowLoad != 'undefined') {
                // we are using core
                hive_cfg_typoscript__windowLoad = true;
              }

              if (typeof hive_cfg_typoscript__windowLoad == 'undefined') {
              } else {
                if (typeof hive_cfg_typoscript__windowLoad == "boolean" && hive_cfg_typoscript__windowLoad) {
                  clearInterval(hive_cpt_cnt_annoying_popup__interval);
                  /**
                   * Fallback for CORE
                   * Not using hive in TYPO3 <=8.6.0
                   *
                   * There is no hive_cfg_typoscript__windowLoad variable available ever!
                   */
                  if (typeof teufels_cfg_typoscript_sStage == 'undefined') {
                  } else {
                    if (teufels_cfg_typoscript_sStage == "prototype" || teufels_cfg_typoscript_sStage == "development") {
                      console.info('jQuery - Annoying Popup - initialize');
                    }
                  }
                  if (typeof hive_cfg_typoscript_sStage == 'undefined') {
                  } else {
                    if (hive_cfg_typoscript_sStage == "prototype" || hive_cfg_typoscript_sStage == "development") {
                      console.info('jQuery - Annoying Popup - initialize');
                    }
                  }
                  if ($('.hive-cpt-cnt-annoying-popup .modal').length > 0) {
                    $('.hive-cpt-cnt-annoying-popup .modal').each(function () {
                      var sId = $(this).attr('id');
                      $('#' + sId).modal();
                      var bLazy = new Blazy({src: 'data-echo', offset: 0, successClass: 'fadeIn'});
                    });
                  }
                }
              }
            }
          }
        }
      }
    }
  },500);



