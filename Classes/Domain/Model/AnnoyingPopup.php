<?php
namespace HIVE\HiveCptCntAnnoyingPopup\Domain\Model;

/***
 *
 * This file is part of the "hive_cpt_cnt_annoying_popup" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017
 *
 ***/

/**
 * AnnoyingPopup
 */
class AnnoyingPopup extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    /**
     * title
     *
     * @var string
     * @validate NotEmpty
     */
    protected $title = '';

    /**
     * image
     *
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     * @cascade remove
     */
    protected $image = null;

    /**
     * text
     *
     * @var string
     * @validate NotEmpty
     */
    protected $text = '';

    /**
     * showOnPages
     *
     * @var string
     * @validate NotEmpty
     */
    protected $showOnPages = '';

    /**
     * cookieLifetime
     *
     * @var int
     * @validate NotEmpty
     */
    protected $cookieLifetime = '';

    /**
     * link
     *
     * @var string
     */
    protected $link = '';

    /**
     * linkLabel
     *
     * @var string
     */
    protected $linkLabel = '';

    /**
     * Returns the title
     *
     * @return string $title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Sets the title
     *
     * @param string $title
     * @return void
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Returns the image
     *
     * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference $image
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Sets the image
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $image
     * @return void
     */
    public function setImage(\TYPO3\CMS\Extbase\Domain\Model\FileReference $image)
    {
        $this->image = $image;
    }

    /**
     * Returns the text
     *
     * @return string $text
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Sets the text
     *
     * @param string $text
     * @return void
     */
    public function setText($text)
    {
        $this->text = $text;
    }

    /**
     * Returns the showOnPages
     *
     * @return string $showOnPages
     */
    public function getShowOnPages()
    {
        return $this->showOnPages;
    }

    /**
     * Sets the showOnPages
     *
     * @param string $showOnPages
     * @return void
     */
    public function setShowOnPages($showOnPages)
    {
        $this->showOnPages = $showOnPages;
    }

    /**
     * Returns the link
     *
     * @return string $link
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * Sets the link
     *
     * @param string $link
     * @return void
     */
    public function setLink($link)
    {
        $this->link = $link;
    }

    /**
     * Returns the cookieLifetime
     *
     * @return int cookieLifetime
     */
    public function getCookieLifetime()
    {
        return $this->cookieLifetime;
    }

    /**
     * Sets the cookieLifetime
     *
     * @param string $cookieLifetime
     * @return void
     */
    public function setCookieLifetime($cookieLifetime)
    {
        $this->cookieLifetime = $cookieLifetime;
    }

    /**
     * Returns the linkLabel
     *
     * @return string $linkLabel
     */
    public function getLinkLabel()
    {
        return $this->linkLabel;
    }

    /**
     * Sets the linkLabel
     *
     * @param string $linkLabel
     * @return void
     */
    public function setLinkLabel($linkLabel)
    {
        $this->linkLabel = $linkLabel;
    }
}
