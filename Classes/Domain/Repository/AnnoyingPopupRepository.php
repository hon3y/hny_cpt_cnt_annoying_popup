<?php
namespace HIVE\HiveCptCntAnnoyingPopup\Domain\Repository;

/***
 *
 * This file is part of the "hive_cpt_cnt_annoying_popup" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017
 *
 ***/

/**
 * The repository for AnnoyingPopups
 */
class AnnoyingPopupRepository extends \HIVE\HiveCfgRepository\Domain\Repository\AbstractRepository
{
    /**
     * @var array
     */
    protected $defaultOrderings = [
        'sorting' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING
    ];

    public function initializeObject()
    {
        $sUserFuncModel = 'HIVE\\HiveCptCntAnnoyingPopup\\Domain\\Model\\AnnoyingPopup';
        $sUserFuncPlugin = 'tx_hivecptcntannoyingpopup';
        parent::overrideQuerySettings($sUserFuncModel, $sUserFuncPlugin);
    }
}
