<?php
declare(strict_types=1);
namespace HIVE\HiveCptCntAnnoyingPopup\Condition\Match;

class CurrentTypo3VersionOperatorGivenVersion
{
    /**
     * @param string $sMatchTypo3VersionAgainst
     * @param string $sOperator
     * @return bool
     */
    public static function match(string $sMatchTypo3VersionAgainst, string $sOperator) : bool {
        /*
         * Use PHP function version_compare
         */
        if (version_compare(TYPO3_version , $sMatchTypo3VersionAgainst, $sOperator)) {
            return true;
        }
        return false;
    }
}