
plugin.tx_hivecptcntannoyingpopup_hivecptcntannoyingpopuprender {
    view {
        # cat=plugin.tx_hivecptcntannoyingpopup_hivecptcntannoyingpopuprender/file; type=string; label=Path to template root (FE)
        templateRootPath = EXT:hive_cpt_cnt_annoying_popup/Resources/Private/Templates/
        # cat=plugin.tx_hivecptcntannoyingpopup_hivecptcntannoyingpopuprender/file; type=string; label=Path to template partials (FE)
        partialRootPath = EXT:hive_cpt_cnt_annoying_popup/Resources/Private/Partials/
        # cat=plugin.tx_hivecptcntannoyingpopup_hivecptcntannoyingpopuprender/file; type=string; label=Path to template layouts (FE)
        layoutRootPath = EXT:hive_cpt_cnt_annoying_popup/Resources/Private/Layouts/
    }
    persistence {
        # cat=plugin.tx_hivecptcntannoyingpopup_hivecptcntannoyingpopuprender//a; type=string; label=Default storage PID
        storagePid =
    }
}

## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder

plugin {
    tx_hivecptcntannoyingpopup {
        model {
            HIVE\HiveCptCntAnnoyingPopup\Domain\Model\AnnoyingPopup {
                persistence {
                    storagePid =
                }
            }
        }
    }
}

plugin.tx_hivecptcntannoyingpopup {
    view {
        templateRootPath = EXT:hive_cpt_cnt_annoying_popup/Resources/Private/Templates/
        partialRootPath = EXT:hive_cpt_cnt_annoying_popup/Resources/Private/Partials/
        layoutRootPath = EXT:hive_cpt_cnt_annoying_popup/Resources/Private/Layouts/
    }
}